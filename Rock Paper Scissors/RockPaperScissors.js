function computerPlay() {
    let choice = Math.floor(Math.random() * 3);
    if (choice === 0) {
        return "Rock";
    }
    else if (choice === 1) {
        return "Paper";
    }
    else
        return "Scissors";
}

function playRound() {
    let userChoice ="";
    let verify = true;
    let draw = true;
    while (draw) {
        while (verify) {
            userChoice = prompt("Choose Rock, Paper, Scissors?");
            userChoice = userChoice.charAt(0).toUpperCase() + userChoice.slice(1).toLowerCase();
            console.log(userChoice);
            if ((userChoice === "Rock") || (userChoice === "Paper") || (userChoice === "Scissors")) {
                verify = false;
            }
            else
                alert("Cheeky, you didn't pick Rock, Paper or Scissors! \n Try again.");

        }

        if (userChoice === "Rock" && console.log(computerPlay()) === "Paper" || "Scissors") {
            return "User you win!";
        }
        else if (userChoice === "Paper" && computerPlay() === "Rock") {
            return "User you win!";

        }
        else if (userChoice === "Paper" && computerPlay() === "Scissors") {
            return "Computer wins!";

        }
        else if (userChoice === "Scissors" && computerPlay() === "Paper") {
            return "User you win!";

        }
        else if (userChoice === "Scissors" && computerPlay() === "Rock") {
            return "Computer wins!";

        }
        else
            alert("It's a draw, go again");
    }
}

function game() {
    let count=0;
    let user, computer;
    while(count<6){
        let result = playRound();
        if(result==="User you win!"){
            user++;
            count++;
            console.log(count);
        }
        else {
            computer++;
            count++;
            console.log(count);
        }
    }
    return alert(user>computer ? alert("User wins!") : alert("Computer Wins!"));

}

game();