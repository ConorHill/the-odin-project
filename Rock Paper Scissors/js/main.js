function computerPlay() {
    let choice = Math.floor(Math.random() * 3);
    if (choice === 0) {
        return "Rock";
    }
    else if (choice === 1) {
        return "Paper";
    }
    else
        return "Scissors";
}

$(document).ready(function () {
    let uScore=0;
    let cScore=0;
    let count=0;
    
    $(".card__rock").click(function () {
        $(".card__rock").effect("bounce", { times: 4 }, 1200);
        if(cScore>=3 || uScore>=3){
            $(".scoreboard__keeper").text("You won that game well done!");
        }
        else if(computerPlay()!="Rock"){
            uScore++;
            $(".results").text("User Wins");
            $(".scoreboard__keeper").text(uScore+" - "+cScore);
            count++;
        }
        else{
            cScore++;
            $(".results").text("Computer Wins");
            $(".scoreboard__keeper").text(uScore+" - "+cScore);
            count++;
        }
    });
    $(".card__paper").click(function () {
        $(".card__paper").effect("bounce", { times: 4}, 1200);
        if(computerPlay()!="Rock"){
            uScore++;
            $(".results").text("User Wins");
            $(".scoreboard__keeper").text(uScore+" - "+cScore);
            count++;
        }
        else{
            cScore++;
            $(".results").text("Computer Wins");
            $(".scoreboard__keeper").text(uScore+" - "+cScore);
            count++;
        }
    });
    $(".card__scissors").click(function () {
        $(".card__scissors").effect("bounce", { times: 4}, 1200);
        if(computerPlay()!="Rock"){
            uScore++;
            $(".results").text("User Wins");
            $(".scoreboard__keeper").text(uScore+" - "+cScore);
            count++;
        }
        else{
            cScore++;
            $(".results").text("Computer Wins");
            $(".scoreboard__keeper").text(uScore+" - "+cScore);
            count++;
        }
    });
});